﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    class Serializator
    {
        public Serializator() { }

        public void Serialize(List<Task> tasks)
        {
            FileStream fs = new FileStream("SerializatorFile.dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();

            try
            {
                formatter.Serialize(fs, tasks);
            }
            catch (SerializationException e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        public List<Task> Deserialize()
        {
            List<Task> task;

            FileStream fs = new FileStream("SerializatorFile.dat", FileMode.Open);

            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                return task = (List<Task>)formatter.Deserialize(fs);
            }
            catch (SerializationException e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }
    }
}
