﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class TaskDbContext: DbContext
    {
        public TaskDbContext(): base("JTTTDataBase")
        {
            //Database.SetInitializer<TaskDbContext>(null);
            Database.SetInitializer<TaskDbContext>(new DropCreateDatabaseIfModelChanges<TaskDbContext>());
        }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<Condition> Conditions { get; set; }
        public DbSet<Action> Actions { get; set; }
    }
}
