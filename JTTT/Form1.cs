﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            using (var ctx = new TaskDbContext())
            {
                int id = 1;
                foreach (var g in ctx.Tasks)
                {
                    if (id != g.TaskID) { id = g.TaskID; }

                    if (g.TaskID == id)
                    {
                        foreach (var a in ctx.Actions)
                        {
                            if (a.ActionID == id)
                            {
                                foreach (var c in ctx.Conditions)
                                {
                                    if (c.ConditionID == id)
                                    {
                                        Condition Con = c;
                                        Action Act = a;
                                        Con.Check();
                                        Task Tsk = new JTTT.Task(Con, Act, g.TaskName);
                                        TaskList.Add(Tsk);
                                        TasksListBox.Items.Add(Tsk.ToString());
                                    }

                                }
                            }
                        }
                    }
                    id++;
                }
            }
        }

        private List<Task> TaskList = new List<Task>();
        private Condition Con;
        private Action Act;

        private void AddToList_Click(object sender, EventArgs e)
        {
            if (TaskNameTextBox.Text == "")
            {
                MessageBox.Show("Nie wpisano nazwy zadania.");
                return;
            }

            //pobranie obrazka ze strony
            if (ConditionTabControl.SelectedIndex == 0)
            {
                if (URLTextBox.Text == "")
                {
                    MessageBox.Show("Nie wpisano adresu URL.");
                    return;
                }
                else
                {
                    if (!Uri.IsWellFormedUriString(URLTextBox.Text, UriKind.Absolute))
                    {
                        MessageBox.Show("Wpisano błędny adres URL.");
                        return;
                    }
                }
                if (WordTextBox.Text == "")
                {
                    MessageBox.Show("Nie wpisano wyrazu.");
                    return;
                }

                Con = new DownloadImage(URLTextBox.Text, WordTextBox.Text);
            }

            //sprawdzenie pogody
            if (ConditionTabControl.SelectedIndex == 1)
            {
                if (CityTextBox.Text == "")
                {
                    MessageBox.Show("Nie wpisano nazwy miasta.");
                    return;
                }

                Con = new CheckWeather(CityTextBox.Text, (int)TemperatureNumericUpDown.Value);

            }

            //wyslanie maila
            if (ActionTabControl.SelectedIndex == 0)
            {
                if (MailTextBox.Text == "")
                {
                    MessageBox.Show("Nie wpisano maila.");
                    return;
                }
                else
                {
                    try
                    {
                        var emailAddress = new System.Net.Mail.MailAddress(MailTextBox.Text);
                    }
                    catch (Exception ex)
                    {
                        while (ex != null)
                        {
                            MessageBox.Show(ex.Message);
                            ex = ex.InnerException;
                        }
                    }
                }

                string MailBody;
                try
                {
                    MailBody = Con.Check();
                }
                catch
                {
                    MessageBox.Show("Nie udało się pobrać nazwy obrazka/warunku pogodowego.");
                    return;
                }

                Act = new SendMail(MailTextBox.Text, MailBody);

                Task Tsk = new Task(Con, Act, TaskNameTextBox.Text);
                TaskList.Add(Tsk);
                TasksListBox.Items.Add(Tsk.ToString());

                using (var ctx = new TaskDbContext())
                {
                    ctx.Tasks.Add(Tsk);
                    ctx.SaveChanges();
                }

            }

            //wyswietlenie
            if (ActionTabControl.SelectedIndex == 1)
            {
                Act = new Display(Con.Check(), Con.PictureURL);

                Task Tsk = new Task(Con, Act, TaskNameTextBox.Text);
                TaskList.Add(Tsk);
                TasksListBox.Items.Add(Tsk.ToString());

                using (var ctx = new TaskDbContext())
                {
                    ctx.Tasks.Add(Tsk);
                    ctx.SaveChanges();
                }
            }
        }

        private void Execute_Click(object sender, EventArgs e)
        {
            foreach (var tsk in TaskList)
            {
                if (tsk.Act.Execute())
                {
                    CreateLogFile logFile = new CreateLogFile(Con.GetType(), Con.getInfo(), Act.GetType(), Act.getInfo());
                    logFile.CreateThis(true);
                }
                else
                {
                    CreateLogFile logFile = new CreateLogFile(Con.GetType(), Con.getInfo(), Act.GetType(), Act.getInfo());
                    logFile.CreateThis(false);
                }
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            TaskList.Clear();
            TasksListBox.Items.Clear();

            using (var ctx = new TaskDbContext())
            {
                foreach (var t in ctx.Tasks)
                {
                    ctx.Tasks.Remove(t); 
                }

                foreach (var a in ctx.Actions)
                {
                    ctx.Actions.Remove(a);
                }

                foreach (var c in ctx.Conditions)
                {
                    ctx.Conditions.Remove(c);
                }
                ctx.SaveChanges();
            }
        }

        private void Serialize_Click(object sender, EventArgs e)
        {
            Serializator s = new Serializator();
            s.Serialize(TaskList);
        }

        private void Deserialize_Click(object sender, EventArgs e)
        {
            Serializator s = new Serializator();

            foreach (var tsk in s.Deserialize())
            {
                TaskList.Add(tsk);

                TasksListBox.Items.Add(tsk.ToString());
            }
        }
    }
}
