﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    class Display : Action
    {
        public string Text { get; set; }
        public string PictureURL { get; set; }

        public Display() { }

        public Display(string Text, string PictureURL)
        {
            this.Text = Text;
            this.PictureURL = PictureURL;
        }

        public override bool Execute()
        {
            Form2 DisplayWindow = new Form2();
            DisplayWindow.DisplayRichTextBox.Text = Text;
            DisplayWindow.DisplayPictureBox.Load(PictureURL);
            DisplayWindow.DisplayPictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
            DisplayWindow.Show();

            return true;
        }

        public override string getInfo()
        {
            return "Message";
        }
    }
}
