﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.IO;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable]
    public class DownloadImage : Condition
    {
        public string Word { get; set; }

        //public ICollection<Task> Tasks { get; set; }

        public DownloadImage(string URL, string Word)
        {
            UrlOrCity = URL;
            this.Word = Word;
        }

        public DownloadImage() { }

        //zwraca nazwe obrazka
        public override string Check()
        {

            //wczytujemy URL w postaci pliku HTML
            var HtmlDoc = new HtmlWeb().Load(UrlOrCity);


            //przeszukujemy opis obrazu (alt) w tagach "img" by znalezc ten z podanym wyrazem
            //i wybieramy jego adres URL
            var nodes = HtmlDoc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {
                if (node.GetAttributeValue("alt", null) != null && node.GetAttributeValue("alt", null).Contains(Word))
                {
                    PictureURL = node.GetAttributeValue("src", null);

                    //zabezpieczenie przed nadpisaniem przy takiej samej nazwie pliku
                    int i = 1;
                    string Temp = Word;
                    while (File.Exists(Word + ".png"))
                    {
                        Word = Temp + i.ToString();
                        ++i;
                    }
                    try
                    {
                        var Web = new WebClient();
                        Web.DownloadFile(PictureURL, Word + ".png");
                        return Word;
                    }
                    catch (Exception Ex)
                    {
                        while (Ex != null)
                        {
                            MessageBox.Show(Ex.Message);
                            Ex = Ex.InnerException;
                        }
                    }
                }
            }
            MessageBox.Show("Nie znaleziono obrazu z podanym wyrazem [" + Word +"].");
            return null;
        }
        public override string getInfo()
        {
            return $"Wyraz: {Word}";
        }
    }
}
