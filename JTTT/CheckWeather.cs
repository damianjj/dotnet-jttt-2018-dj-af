﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class CheckWeather : Condition
    {
        public int tempThreshold { set; get; }
        public WeatherObj Wthr;

        public CheckWeather() { }

        public CheckWeather(string city, int tempThreshold)
        {
            this.tempThreshold = tempThreshold;
            UrlOrCity = city;
        }

        public override string Check()
        {
            string apiLink = "http://api.openweathermap.org/data/2.5/weather?q=" + UrlOrCity + ",pl&APPID=520a632fd08ad74c22e3cb7813e2e36a&units=metric&lang=pl";
            string json;

            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                json = WebUtility.HtmlDecode(wc.DownloadString(apiLink));
            }

            Wthr = JsonConvert.DeserializeObject<WeatherObj>(json);

            if (Wthr.Main.Temp > tempThreshold)
            {
                PictureURL = "http://openweathermap.org/img/w/" + Wthr.Weather[0].Icon + ".png";
                return $"Temperatura wysoka! Aktualnie w mieście {Wthr.Name} temperatura wynosi: {Wthr.Main.Temp}*C";
            }

            //DisplayRichTextBox.Text = $"Miasto: {Wthr.Name}\nTemperatura: {Wthr.Main.Temp}*C\nCiśnienie: {Wthr.Main.Pressure}hPa\nWilgotność: {Wthr.Main.Humidity}%\nZachmurzenie: {Wthr.Clouds.All}%\nPrędkość wiatru: {Wthr.Wind.Speed}m/s\nNiebo: {Wthr.Weather[0].Description}.";
            
            return null;
        }
        public override string getInfo()
        {
            return $"{Wthr.Name} " + tempThreshold.ToString() + "*C";
        }
    }
}
