﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Windows.Forms;
using System.IO;

namespace JTTT
{
    [Serializable]
    public class SendMail : Action
    {
        public string Mail { get; set; }
        public string MailBody { get; set; }

        //public ICollection<Task> Tasks { get; set; }

        public SendMail() { }

        public SendMail(string Mail, string MailBody)
        {
            this.Mail = Mail;
            this.MailBody = MailBody;
        }

        public override bool Execute()
        {
            MailMessage MailMsg = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            MailMsg.From = new MailAddress("testowymail19@gmail.com");
            MailMsg.To.Add(Mail);
            MailMsg.Subject = "JTTT: Wiadomość";
            MailMsg.Body = MailBody;

            if (File.Exists(MailBody + ".png"))
            {
                System.Net.Mail.Attachment Attachment;
                Attachment = new System.Net.Mail.Attachment(MailBody + ".png");
                MailMsg.Attachments.Add(Attachment);
            }

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("testowymail19", "testowymail");
            SmtpServer.EnableSsl = true;

            try
            {
                SmtpServer.Send(MailMsg);
                return true;
            }
            catch (Exception Ex)
            {
                while (Ex != null)
                {
                    MessageBox.Show(Ex.Message);
                    Ex = Ex.InnerException;
                }
            }
            return false;
        }

        public override string getInfo()
        {
            return Mail;
        }
    }
}
