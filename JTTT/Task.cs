﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable]
    public class Task
    {
        public int TaskID { get; set; }
        public string TaskName { get; set; }
        public virtual Action Act { get; set; }
        public virtual Condition Con { get; set; }

        public Task() { }

        public Task(Condition con, Action act, string taskName)
        {
            Act = act;
            Con = con;
            TaskName = taskName;
        }

        public override string ToString()
        {
            return $"taskName={TaskName} || Condition={Con.GetType()}: {Con.getInfo()} || Action={Act.GetType()}: {Act.getInfo()}";
        }
    }
}
