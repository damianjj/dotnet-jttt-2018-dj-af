﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Condition
    {
        public string UrlOrCity { get; set; }
        public string PictureURL { get; set; }

        public int ConditionID { get; set; }
        public ICollection<Task> Tasks { get; set; }

        public virtual string Check()
        {
            return null;
        }
        public virtual string getInfo()
        {
            return null;
        }
    }
}
