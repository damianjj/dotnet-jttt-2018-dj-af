﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddButton = new System.Windows.Forms.Button();
            this.URLTextBox = new System.Windows.Forms.TextBox();
            this.WordTextBox = new System.Windows.Forms.TextBox();
            this.MailTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ExecuteButton = new System.Windows.Forms.Button();
            this.TasksListBox = new System.Windows.Forms.ListBox();
            this.ClearButton = new System.Windows.Forms.Button();
            this.TaskNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SerializeButton = new System.Windows.Forms.Button();
            this.DeserializeButton = new System.Windows.Forms.Button();
            this.ConditionTabControl = new System.Windows.Forms.TabControl();
            this.DownloadImageTabPage = new System.Windows.Forms.TabPage();
            this.WeatherCheckTabPage = new System.Windows.Forms.TabPage();
            this.TemperatureNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.Degrees = new System.Windows.Forms.Label();
            this.CityTextBox = new System.Windows.Forms.TextBox();
            this.TemperatureLabel = new System.Windows.Forms.Label();
            this.CityLabel = new System.Windows.Forms.Label();
            this.ActionTabControl = new System.Windows.Forms.TabControl();
            this.SendMailTabPage = new System.Windows.Forms.TabPage();
            this.DisplayTabPage = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.ConditionTabControl.SuspendLayout();
            this.DownloadImageTabPage.SuspendLayout();
            this.WeatherCheckTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureNumericUpDown)).BeginInit();
            this.ActionTabControl.SuspendLayout();
            this.SendMailTabPage.SuspendLayout();
            this.DisplayTabPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(172, 329);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(118, 23);
            this.AddButton.TabIndex = 0;
            this.AddButton.Text = "Dodaj do listy";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddToList_Click);
            // 
            // URLTextBox
            // 
            this.URLTextBox.Location = new System.Drawing.Point(46, 19);
            this.URLTextBox.Name = "URLTextBox";
            this.URLTextBox.Size = new System.Drawing.Size(339, 20);
            this.URLTextBox.TabIndex = 1;
            // 
            // WordTextBox
            // 
            this.WordTextBox.Location = new System.Drawing.Point(46, 52);
            this.WordTextBox.Name = "WordTextBox";
            this.WordTextBox.Size = new System.Drawing.Size(339, 20);
            this.WordTextBox.TabIndex = 2;
            // 
            // MailTextBox
            // 
            this.MailTextBox.Location = new System.Drawing.Point(41, 19);
            this.MailTextBox.Name = "MailTextBox";
            this.MailTextBox.Size = new System.Drawing.Size(347, 20);
            this.MailTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "URL:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tekst:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Mail:";
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Location = new System.Drawing.Point(541, 304);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(118, 48);
            this.ExecuteButton.TabIndex = 7;
            this.ExecuteButton.Text = "Wykonaj!";
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.Execute_Click);
            // 
            // TasksListBox
            // 
            this.TasksListBox.FormattingEnabled = true;
            this.TasksListBox.Location = new System.Drawing.Point(484, 49);
            this.TasksListBox.Name = "TasksListBox";
            this.TasksListBox.Size = new System.Drawing.Size(490, 238);
            this.TasksListBox.TabIndex = 8;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(665, 304);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(118, 48);
            this.ClearButton.TabIndex = 9;
            this.ClearButton.Text = "Czyść!";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.Clear_Click);
            // 
            // TaskNameTextBox
            // 
            this.TaskNameTextBox.Location = new System.Drawing.Point(199, 304);
            this.TaskNameTextBox.Name = "TaskNameTextBox";
            this.TaskNameTextBox.Size = new System.Drawing.Size(126, 20);
            this.TaskNameTextBox.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(113, 307);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Nazwa zadania:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(176, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 39);
            this.label5.TabIndex = 12;
            this.label5.Text = "Jeżeli:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(192, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 39);
            this.label6.TabIndex = 13;
            this.label6.Text = "To:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(640, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(204, 39);
            this.label7.TabIndex = 14;
            this.label7.Text = "Lista zadań:";
            // 
            // SerializeButton
            // 
            this.SerializeButton.Location = new System.Drawing.Point(789, 304);
            this.SerializeButton.Name = "SerializeButton";
            this.SerializeButton.Size = new System.Drawing.Size(118, 23);
            this.SerializeButton.TabIndex = 15;
            this.SerializeButton.Text = "Serializuj!";
            this.SerializeButton.UseVisualStyleBackColor = true;
            this.SerializeButton.Click += new System.EventHandler(this.Serialize_Click);
            // 
            // DeserializeButton
            // 
            this.DeserializeButton.Location = new System.Drawing.Point(789, 329);
            this.DeserializeButton.Name = "DeserializeButton";
            this.DeserializeButton.Size = new System.Drawing.Size(118, 23);
            this.DeserializeButton.TabIndex = 16;
            this.DeserializeButton.Text = "Deserializuj!";
            this.DeserializeButton.UseVisualStyleBackColor = true;
            this.DeserializeButton.Click += new System.EventHandler(this.Deserialize_Click);
            // 
            // ConditionTabControl
            // 
            this.ConditionTabControl.Controls.Add(this.DownloadImageTabPage);
            this.ConditionTabControl.Controls.Add(this.WeatherCheckTabPage);
            this.ConditionTabControl.Location = new System.Drawing.Point(19, 49);
            this.ConditionTabControl.Name = "ConditionTabControl";
            this.ConditionTabControl.SelectedIndex = 0;
            this.ConditionTabControl.Size = new System.Drawing.Size(410, 112);
            this.ConditionTabControl.TabIndex = 18;
            // 
            // DownloadImageTabPage
            // 
            this.DownloadImageTabPage.Controls.Add(this.URLTextBox);
            this.DownloadImageTabPage.Controls.Add(this.WordTextBox);
            this.DownloadImageTabPage.Controls.Add(this.label1);
            this.DownloadImageTabPage.Controls.Add(this.label2);
            this.DownloadImageTabPage.Location = new System.Drawing.Point(4, 22);
            this.DownloadImageTabPage.Name = "DownloadImageTabPage";
            this.DownloadImageTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.DownloadImageTabPage.Size = new System.Drawing.Size(402, 86);
            this.DownloadImageTabPage.TabIndex = 0;
            this.DownloadImageTabPage.Text = "Sprawdź stronę";
            this.DownloadImageTabPage.UseVisualStyleBackColor = true;
            // 
            // WeatherCheckTabPage
            // 
            this.WeatherCheckTabPage.Controls.Add(this.TemperatureNumericUpDown);
            this.WeatherCheckTabPage.Controls.Add(this.Degrees);
            this.WeatherCheckTabPage.Controls.Add(this.CityTextBox);
            this.WeatherCheckTabPage.Controls.Add(this.TemperatureLabel);
            this.WeatherCheckTabPage.Controls.Add(this.CityLabel);
            this.WeatherCheckTabPage.Location = new System.Drawing.Point(4, 22);
            this.WeatherCheckTabPage.Name = "WeatherCheckTabPage";
            this.WeatherCheckTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.WeatherCheckTabPage.Size = new System.Drawing.Size(402, 86);
            this.WeatherCheckTabPage.TabIndex = 1;
            this.WeatherCheckTabPage.Text = "Sprawdź pogodę";
            this.WeatherCheckTabPage.UseVisualStyleBackColor = true;
            // 
            // TemperatureNumericUpDown
            // 
            this.TemperatureNumericUpDown.Location = new System.Drawing.Point(79, 52);
            this.TemperatureNumericUpDown.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.TemperatureNumericUpDown.Name = "TemperatureNumericUpDown";
            this.TemperatureNumericUpDown.Size = new System.Drawing.Size(31, 20);
            this.TemperatureNumericUpDown.TabIndex = 21;
            // 
            // Degrees
            // 
            this.Degrees.AutoSize = true;
            this.Degrees.Location = new System.Drawing.Point(112, 55);
            this.Degrees.Name = "Degrees";
            this.Degrees.Size = new System.Drawing.Size(18, 13);
            this.Degrees.TabIndex = 20;
            this.Degrees.Text = "*C";
            // 
            // CityTextBox
            // 
            this.CityTextBox.Location = new System.Drawing.Point(79, 19);
            this.CityTextBox.Name = "CityTextBox";
            this.CityTextBox.Size = new System.Drawing.Size(306, 20);
            this.CityTextBox.TabIndex = 6;
            // 
            // TemperatureLabel
            // 
            this.TemperatureLabel.AutoSize = true;
            this.TemperatureLabel.Location = new System.Drawing.Point(3, 55);
            this.TemperatureLabel.Name = "TemperatureLabel";
            this.TemperatureLabel.Size = new System.Drawing.Size(70, 13);
            this.TemperatureLabel.TabIndex = 9;
            this.TemperatureLabel.Text = "Temperatura:";
            // 
            // CityLabel
            // 
            this.CityLabel.AutoSize = true;
            this.CityLabel.Location = new System.Drawing.Point(3, 22);
            this.CityLabel.Name = "CityLabel";
            this.CityLabel.Size = new System.Drawing.Size(41, 13);
            this.CityLabel.TabIndex = 8;
            this.CityLabel.Text = "Miasto:";
            // 
            // ActionTabControl
            // 
            this.ActionTabControl.Controls.Add(this.SendMailTabPage);
            this.ActionTabControl.Controls.Add(this.DisplayTabPage);
            this.ActionTabControl.Location = new System.Drawing.Point(19, 206);
            this.ActionTabControl.Name = "ActionTabControl";
            this.ActionTabControl.SelectedIndex = 0;
            this.ActionTabControl.Size = new System.Drawing.Size(410, 81);
            this.ActionTabControl.TabIndex = 19;
            // 
            // SendMailTabPage
            // 
            this.SendMailTabPage.Controls.Add(this.label3);
            this.SendMailTabPage.Controls.Add(this.MailTextBox);
            this.SendMailTabPage.Location = new System.Drawing.Point(4, 22);
            this.SendMailTabPage.Name = "SendMailTabPage";
            this.SendMailTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.SendMailTabPage.Size = new System.Drawing.Size(402, 55);
            this.SendMailTabPage.TabIndex = 0;
            this.SendMailTabPage.Text = "Wyślij maila";
            this.SendMailTabPage.UseVisualStyleBackColor = true;
            // 
            // DisplayTabPage
            // 
            this.DisplayTabPage.Controls.Add(this.label8);
            this.DisplayTabPage.Location = new System.Drawing.Point(4, 22);
            this.DisplayTabPage.Name = "DisplayTabPage";
            this.DisplayTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.DisplayTabPage.Size = new System.Drawing.Size(402, 55);
            this.DisplayTabPage.TabIndex = 1;
            this.DisplayTabPage.Text = "Wyświetl";
            this.DisplayTabPage.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 22);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(142, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Pokaż komunikat na ekranie";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(997, 369);
            this.Controls.Add(this.ActionTabControl);
            this.Controls.Add(this.ConditionTabControl);
            this.Controls.Add(this.DeserializeButton);
            this.Controls.Add(this.SerializeButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TaskNameTextBox);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.TasksListBox);
            this.Controls.Add(this.ExecuteButton);
            this.Controls.Add(this.AddButton);
            this.Name = "Form1";
            this.Text = "JTTT";
            this.ConditionTabControl.ResumeLayout(false);
            this.DownloadImageTabPage.ResumeLayout(false);
            this.DownloadImageTabPage.PerformLayout();
            this.WeatherCheckTabPage.ResumeLayout(false);
            this.WeatherCheckTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TemperatureNumericUpDown)).EndInit();
            this.ActionTabControl.ResumeLayout(false);
            this.SendMailTabPage.ResumeLayout(false);
            this.SendMailTabPage.PerformLayout();
            this.DisplayTabPage.ResumeLayout(false);
            this.DisplayTabPage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.TextBox URLTextBox;
        private System.Windows.Forms.TextBox WordTextBox;
        private System.Windows.Forms.TextBox MailTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button ExecuteButton;
        private System.Windows.Forms.ListBox TasksListBox;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox TaskNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button SerializeButton;
        private System.Windows.Forms.Button DeserializeButton;
        private System.Windows.Forms.TabControl ConditionTabControl;
        private System.Windows.Forms.TabPage DownloadImageTabPage;
        private System.Windows.Forms.TabPage WeatherCheckTabPage;
        private System.Windows.Forms.TextBox CityTextBox;
        private System.Windows.Forms.Label TemperatureLabel;
        private System.Windows.Forms.Label CityLabel;
        private System.Windows.Forms.Label Degrees;
        private System.Windows.Forms.TabControl ActionTabControl;
        private System.Windows.Forms.TabPage SendMailTabPage;
        private System.Windows.Forms.TabPage DisplayTabPage;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown TemperatureNumericUpDown;
    }
}

