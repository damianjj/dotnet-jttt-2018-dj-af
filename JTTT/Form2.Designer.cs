﻿namespace JTTT
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DisplayRichTextBox = new System.Windows.Forms.RichTextBox();
            this.DisplayPictureBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DisplayPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DisplayRichTextBox
            // 
            this.DisplayRichTextBox.Location = new System.Drawing.Point(12, 31);
            this.DisplayRichTextBox.Name = "DisplayRichTextBox";
            this.DisplayRichTextBox.Size = new System.Drawing.Size(592, 94);
            this.DisplayRichTextBox.TabIndex = 2;
            this.DisplayRichTextBox.Text = "";
            // 
            // DisplayPictureBox
            // 
            this.DisplayPictureBox.Location = new System.Drawing.Point(12, 131);
            this.DisplayPictureBox.Name = "DisplayPictureBox";
            this.DisplayPictureBox.Size = new System.Drawing.Size(592, 282);
            this.DisplayPictureBox.TabIndex = 3;
            this.DisplayPictureBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Opis";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 425);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DisplayPictureBox);
            this.Controls.Add(this.DisplayRichTextBox);
            this.Name = "Form2";
            this.Text = "Wyświetlanie";
            ((System.ComponentModel.ISupportInitialize)(this.DisplayPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RichTextBox DisplayRichTextBox;
        public System.Windows.Forms.PictureBox DisplayPictureBox;
        private System.Windows.Forms.Label label1;
    }
}