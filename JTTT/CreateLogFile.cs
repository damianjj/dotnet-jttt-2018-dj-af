﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    class CreateLogFile
    {
        private System.Type _conditionType;
        private string _conditionValue;
        private System.Type _actionType;
        private string _actionValue;

        DateTime thisDay = DateTime.Now;

        public CreateLogFile(System.Type conditionType, string conditionValue, System.Type actionType, string actionValue)
        {
            _conditionType = conditionType;
            _conditionValue = conditionValue;
            _actionType = actionType;
            _actionValue = actionValue;
        }

        public void CreateThis(bool succes)
        {
            if (succes)
            {
                using (StreamWriter file = new StreamWriter("LogFile.log", true))
                {
                    file.WriteLine("**********");
                    file.WriteLine(thisDay.ToString());
                    file.WriteLine("Typ warunku: " + _conditionType);
                    file.WriteLine("Wartość warunku: " + _conditionValue);
                    file.WriteLine("Typ akcji: " + _actionType);
                    file.WriteLine("Wartość akcji: " + _actionValue);
                    file.WriteLine("Sukces.");
                    file.WriteLine("**********");
                }
            }
            else
            {
                using (StreamWriter file = new StreamWriter("LogFile.log", true))
                {
                    file.WriteLine("**********");
                    file.WriteLine(thisDay.ToString());
                    file.WriteLine("Typ warunku: " + _conditionType);
                    file.WriteLine("Wartość warunku: " + _conditionValue);
                    file.WriteLine("Typ akcji: " + _actionType);
                    file.WriteLine("Wartość akcji: " + _actionValue);
                    file.WriteLine("Niepowodzenie.");
                    file.WriteLine("**********");
                }
            }
        }
    }
}
