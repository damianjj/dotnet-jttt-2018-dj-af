﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT
{
    public class Action
    {
        public int ActionID { get; set; }
        public ICollection<Task> Tasks { get; set; }

        public virtual bool Execute()
        {
            return false;
        }
        public virtual string getInfo()
        {
            return null;
        }
    }
}
